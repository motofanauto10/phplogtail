tail -f implementation with php
===============================

How to use:

logtailtest.php
---------------

.. code-block:: php

	<?php

	require_once '../src/LogTail/LogTailKernel.php';

	class LogTailTest extends \LogTail\LogTailKernel{

		protected function parse($value){
			echo $value . "-test\n";
		}
	}

	new LogTailTest('data.txt');

Launch the script in terminal and save new lines on data.txt, you will see the lines appear
 
