<?php

require_once '../src/LogTail/LogTailKernel.php';

if(empty($_SERVER['argv'][1])){
	die('argv 1 must be "data.txt"'."\n");
}

class LogTailTest extends \LogTail\LogTailKernel{
	
	protected function parse($value){
		echo $value . "-test\n";
	}
}

new LogTailTest($_SERVER['argv'][1], __DIR__ .'/../metadata');
 
