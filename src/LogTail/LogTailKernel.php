<?php

namespace LogTail;

/**
 * Description of LogTailKernel
 *
 * @author jmon
 */
class LogTailKernel {
	
	/**
	 * Location of the log file
	 * @var string
	 */
	protected $_log;
	/**
	 * Updates
	 * @var integer
	 */
	protected $_update_time;
	/**
	 * This variable holds the last amount of parsed data
	 * @var int
	 */
	protected $_last_fetched_size = 0;
	/**
	 * @var integer new size to process 
	 */
	protected $_process_size = 0;
	/**
	 * @var boolean whiel loop condition
	 */
	protected $_stop = false;
	/**
	 * @var string folder path with logs metadata 
	 */
	protected $_metadata_folder;

	/**
	 * 
	 * LogTail constructor
	 * @param string $log the location of the log file
	 * @param string $metadata_folder folder where save metadata information of parsed logs. 
	 * @param integer $update_time The time between KERNEL iterations SECS. 
	 */
	public function __construct($log, $metadata_folder = false ,$update_time = 1) {
		
		register_shutdown_function(array($this, 'shutdown'));
		
		if(!file_exists($log)){
			throw new Exception('File "'.$log.'" does not exist!');
		}
		
		$this->_log = $log;
		$this->_update_time = $update_time;
		
		if($metadata_folder){
			if(!file_exists($metadata_folder)){
				throw new Exception('Metadata folder "'.$metadata_folder.'" does not exist!');
			}else{
				$metadata_folder = realpath($metadata_folder) . '/';
			}
		}
		$this->_metadata_folder = $metadata_folder;
		
		$this->initStat();
		$this->doLoop($update_time);
	}
	
	protected function doLoop($update_time){
		
		while(!$this->_stop){
			$this->getNewLines();
			sleep($update_time);
		}
	}
	
	protected function getNewLines(){
		
		$this->setStat();
		/**
		 * Actually load the data
		 */
		$data = array();
		if($this->_process_size > 0) {
			
			$this->fp = fopen($this->_log, 'r');
			fseek($this->fp, -$this->_process_size , SEEK_END);
			$data = explode("\n", fread($this->fp, $this->_process_size));
			fclose($this->fp);
			
		}
		
		foreach ($data as $value) {
			if(!empty($value)){
				$this->parseData($value);
			}	
		}
	}
	
	protected function shutdown(){
		$this->_stop = true;
	}
	
	protected function parseData($value){
		echo $value . "\n";
	}
	
	protected function setStat(){
		
		clearstatcache();
		
		$hash = md5($this->_log);
		
		$stat = stat($this->_log);

		if($this->_last_fetched_size  <= $stat['size']) {
			
			$this->_process_size = ($stat['size'] - $this->_last_fetched_size);
			
		}else{
			
			$this->_process_size = $stat['size'];
		}
		$this->_last_fetched_size = $stat['size'];
		
		if($this->_metadata_folder){
			$metadata = array(
				'last_fetched_size'	=> $this->_last_fetched_size
			);

			file_put_contents($this->_metadata_folder . $hash, serialize($metadata));
		}
	}
	
	protected function initStat(){
		
		$hash = md5($this->_log);
		
		if($this->_metadata_folder && file_exists($this->_metadata_folder . $hash)){
			$content = unserialize( file_get_contents( $this->_metadata_folder . $hash ) );
			$this->_last_fetched_size = $content['last_fetched_size'];
		}
	}
}
